def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  arr.inject(0, :+)
end

def power(a, b)
  a ** b
end
def mult(*args)
  args.reduce(:*)
end

def factorial(f)
  return 1 if f == 0
  (1..f).inject(:*)
end
