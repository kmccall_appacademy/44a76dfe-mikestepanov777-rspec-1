def translate(text)
  arr = text.split(" ")
  arr.map! do |str|
    punctuation = str.scan(/\W/)
    if str.capitalize == str
      rotate(str.downcase.chars - punctuation).capitalize + punctuation.join
    else
      rotate(str.downcase.chars - punctuation) + punctuation.join
    end
  end
  arr.join(" ")
end

def rotate(arr)
  vowels = ["a", "e", "i", "o", "u"]
  idx = 0
  while idx < arr.size
    if vowels.include?(arr[0])
      break
    elsif arr[0] == "q" && arr[1] == "u"
      arr.rotate!(2)
      idx += 2
    else
      arr.rotate!(1)
      idx += 1
    end
  end
  arr.join + "ay"
end
