def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  Array.new(times, str).join(" ")
end

def start_of_word(str, range = 1)
  str[0...range]
end

def first_word(str)
  str.scan(/\w+/).first
end

def titleize(str)
  little_words = ["and", "the", "over"]
  arr = str.split(" ")
  arr.map! {|str| little_words.include?(str) ? str : str.capitalize}
  arr.first.capitalize!
  arr.last.capitalize!
  arr.join(" ")
end
